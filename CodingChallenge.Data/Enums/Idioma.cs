﻿namespace CodingChallenge.Data.Enums
{
    public enum Idioma
    {
        Castellano,
        Ingles,
        Frances
    }
}
