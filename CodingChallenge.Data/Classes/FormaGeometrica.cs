﻿/*
 * Refactorear la clase para respetar principios de programación orientada a objetos. Qué pasa si debemos soportar un nuevo idioma para los reportes, o
 * agregar más formas geométricas?
 *
 * Se puede hacer cualquier cambio que se crea necesario tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Trapecio/Rectangulo, agregar otro idioma a reporting.
 * */

using System;
using CodingChallenge.Data.Enums;

namespace CodingChallenge.Data.Classes
{
    public abstract class FormaGeometrica
    {
        private readonly TipoForma _tipoForma;
        public virtual TipoForma TipoForma => _tipoForma;

        protected readonly decimal _lado;

        public FormaGeometrica(TipoForma tipoForma, decimal ancho)
        {
            _tipoForma = tipoForma;
            _lado = ancho;
        }
        
        public virtual decimal CalcularArea()
        {
            throw new ArgumentOutOfRangeException(@"Forma desconocida");
        }

        public virtual decimal CalcularPerimetro()
        {
            throw new ArgumentOutOfRangeException(@"Forma desconocida");
        }
    }
}
