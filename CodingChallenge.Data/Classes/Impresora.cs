﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodingChallenge.Data.Enums;

namespace CodingChallenge.Data.Classes
{
    public static class Impresora
    {
        /// <summary>
        /// Supported printable shapes.
        /// </summary>
        public static IList<TipoForma> FormasSoportadas = new List<TipoForma>
        {
            TipoForma.Cuadrado,
            TipoForma.Circulo,
            TipoForma.Rectangulo,
            TipoForma.TrianguloEquilatero
        };

        public static string Imprimir(IList<FormaGeometrica> formas, Idioma idioma)
        {
            var sb = new StringBuilder();

            var formasNoSoportadas = formas.Select(f => f.TipoForma).Where(t => !FormasSoportadas.Contains(t));
            if (formasNoSoportadas.Any())
            {
                throw new System.Exception($"Intentando imprimir tipo de forma/s no soportada/s: {formasNoSoportadas}");
            }

            if (!formas.Any())
            {
                sb.Append($"<h1>{Mensajes.Diccionarios[idioma][Mensajes.Clave.EmptyList]}</h1>");
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                sb.Append($"<h1>{Mensajes.Diccionarios[idioma][Mensajes.Clave.ImpresoraHeader]}</h1>");

                var numeroFormas = new Dictionary<TipoForma, int>();
                foreach (var formaSoportada in FormasSoportadas)
                {
                    numeroFormas.Add(formaSoportada, 0);
                }

                var areaFormas = new Dictionary<TipoForma, decimal>();
                foreach (var formaSoportada in FormasSoportadas)
                {
                    areaFormas.Add(formaSoportada, 0);
                }

                var perimetroFormas = new Dictionary<TipoForma, decimal>();
                foreach (var formaSoportada in FormasSoportadas)
                {
                    perimetroFormas.Add(formaSoportada, 0);
                }

                for (var i = 0; i < formas.Count; i++)
                {
                    var tipo = formas[i].TipoForma;
                    numeroFormas[tipo]++;
                    areaFormas[tipo] += formas[i].CalcularArea();
                    perimetroFormas[tipo] += formas[i].CalcularPerimetro();
                }

                foreach (var formaSoportada in FormasSoportadas)
                {
                    sb.Append(ObtenerLinea(numeroFormas[formaSoportada],
                    areaFormas[formaSoportada],
                    perimetroFormas[formaSoportada],
                    formaSoportada, idioma));
                }

                // FOOTER
                var totalFormas = numeroFormas.Sum(f => f.Value);
                var areaTotal = areaFormas.Sum(f => f.Value);
                var perimetroTotal = perimetroFormas.Sum(f => f.Value);

                sb.Append("TOTAL:<br/>");
                sb.Append($"{totalFormas} {Mensajes.Diccionarios[idioma][Mensajes.Clave.Shapes]} ");
                sb.Append($"{Mensajes.Diccionarios[idioma][Mensajes.Clave.Perimeter]} {perimetroTotal.ToString("#.##")} ");
                sb.Append($"{Mensajes.Diccionarios[idioma][Mensajes.Clave.Area]} {areaTotal.ToString("#.##")}");
            }

            return sb.ToString();
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, TipoForma tipo, Idioma idioma)
        {
            if (cantidad > 0)
            {
                return $"{cantidad} {TraducirForma(tipo, cantidad, idioma)} | {Mensajes.Diccionarios[idioma][Mensajes.Clave.Area]} {area:#.##} | {Mensajes.Diccionarios[idioma][Mensajes.Clave.Perimeter]} {perimetro:#.##} <br/>";
            }

            return string.Empty;
        }

        private static string TraducirForma(TipoForma tipo, int cantidad, Idioma idioma)
        {
            switch (tipo)
            {
                case TipoForma.Cuadrado:
                    return cantidad == 1
                            ? Mensajes.Diccionarios[idioma][Mensajes.Clave.Square]
                            : Mensajes.Diccionarios[idioma][Mensajes.Clave.Squares];
                case TipoForma.Circulo:
                    return cantidad == 1
                        ? Mensajes.Diccionarios[idioma][Mensajes.Clave.Circle]
                        : Mensajes.Diccionarios[idioma][Mensajes.Clave.Circles];
                case TipoForma.TrianguloEquilatero:
                    return cantidad == 1
                        ? Mensajes.Diccionarios[idioma][Mensajes.Clave.Triangle]
                        : Mensajes.Diccionarios[idioma][Mensajes.Clave.Triangles];
                case TipoForma.Rectangulo:
                    return cantidad == 1
                        ? Mensajes.Diccionarios[idioma][Mensajes.Clave.Rectangle]
                        : Mensajes.Diccionarios[idioma][Mensajes.Clave.Rectangles];
            }

            return string.Empty;
        }
    }
}
