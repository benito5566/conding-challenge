﻿using System;
using CodingChallenge.Data.Enums;

namespace CodingChallenge.Data.Classes
{
    public static class FabricaFormaGeometrica
    {
        public static FormaGeometrica Fabricar(TipoForma forma, decimal ancho, decimal? alto = null)
        {
            switch (forma)
            {
                case TipoForma.Cuadrado:
                    return new Cuadrado(ancho);
                case TipoForma.TrianguloEquilatero:
                    return new TrianguloEquilatero(ancho);
                case TipoForma.Circulo:
                    return new Circulo(ancho);
                case TipoForma.Rectangulo:
                    return new Rectangulo(ancho, alto.Value);
                default:
                    throw new Exception($"Forma no soportada {forma}");
            }
        }
    }
}
