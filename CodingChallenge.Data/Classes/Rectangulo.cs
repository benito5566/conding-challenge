﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodingChallenge.Data.Enums;

namespace CodingChallenge.Data.Classes
{
    public class Rectangulo : FormaGeometrica
    {
        private readonly decimal _alto;

        public Rectangulo(decimal ancho, decimal alto) : base(TipoForma.Rectangulo, ancho)
        {
            _alto = alto;
        }

        public override decimal CalcularArea()
        {
            return _alto * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _alto * 2 + _lado * 2;
        }
    }
}
