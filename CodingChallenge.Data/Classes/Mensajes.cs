﻿using System.Collections.Generic;
using CodingChallenge.Data.Enums;

namespace CodingChallenge.Data.Classes
{
    public static class Mensajes
    {
        /// <summary>
        /// These are the keys used to obtain a the correct message from the dictionaries:
        /// <seealso cref="Mensajes.MensajesIngles"/> and <seealso cref="Mensajes.MensajesCastellano"/>
        /// </summary>
        /// <remarks> If you add and entry here, remember to add it to all dictionaries.</remarks>
        public static class Clave
        {
            public const string EmptyList = "EmptyList";
            public const string ImpresoraHeader = "Impresora.Header";
            public const string Square = "Square";
            // Should be a better way to pluralize, but using this way for simplity.
            public const string Squares = "Squares";
            public const string Triangle = "Triangle";
            public const string Triangles = "Triangles";
            public const string Circle = "Circle";
            public const string Circles = "Circles";
            public const string Area = "Area";
            public const string Perimeter = "Perimeter";
            public const string Shapes = "Shapes";
            public const string Rectangle = "Rectangle";
            public const string Rectangles = "Rectangles";

        }

        public static IReadOnlyDictionary<string, string> MensajesIngles = new Dictionary<string, string>
        {
            { Clave.EmptyList,"Empty list of shapes!" },
            { Clave.ImpresoraHeader,"Shapes report" },
            { Clave.Square, "Square" },
            { Clave.Squares, "Squares" },
            { Clave.Triangle, "Triangle" },
            { Clave.Triangles, "Triangles" },
            { Clave.Circle, "Circle" },
            { Clave.Circles, "Circles" },
            { Clave.Area, "Area" },
            { Clave.Perimeter, "Perimeter" },
            { Clave.Shapes, "shapes"},
            { Clave.Rectangle, "Rectangle" },
            { Clave.Rectangles, "Rectangles" }
        };

        public static IReadOnlyDictionary<string,string> MensajesFrances = new Dictionary<string, string>
        {
            { Clave.EmptyList,"La liste de formes est vide.!" },
            { Clave.ImpresoraHeader,"Rapport de formes géométriques" },
            { Clave.Square, "Carre" },
            { Clave.Squares, "Carres" },
            { Clave.Triangle, "Triangle" },
            { Clave.Triangles, "Triangles" },
            { Clave.Circle, "Cercle" },
            { Clave.Circles, "Cercles" },
            { Clave.Area, "Surface" },
            { Clave.Perimeter, "Périmètre" },
            { Clave.Shapes, "formes"},
            { Clave.Rectangle, "Rectangle" },
            { Clave.Rectangles, "Rectangles" }
        };

        public static IReadOnlyDictionary<string, string> MensajesCastellano = new Dictionary<string, string>
        {
            { Clave.EmptyList,"Lista vacía de formas!" },
            { Clave.ImpresoraHeader,"Reporte de Formas" },
            { Clave.Square,"Cuadrado" },
            { Clave.Squares,"Cuadrados" },
            { Clave.Triangle,"Triángulo" },
            { Clave.Triangles,"Triángulos" },
            { Clave.Circle,"Círculo" },
            { Clave.Circles,"Círculos" },
            { Clave.Area, "Area" },
            { Clave.Perimeter, "Perimetro"},
            { Clave.Shapes, "formas" },
            { Clave.Rectangle, "Rectángulo" },
            { Clave.Rectangles, "Rectángulos" }
        };

        public static IReadOnlyDictionary<Idioma, IReadOnlyDictionary<string, string>> Diccionarios = new Dictionary<Idioma, IReadOnlyDictionary<string, string>>
        {
            { Idioma.Ingles, MensajesIngles },
            { Idioma.Castellano, MensajesCastellano },
            { Idioma.Frances, MensajesFrances}
        };
    }
}
