﻿using CodingChallenge.Data.Enums;

namespace CodingChallenge.Data.Classes
{
    public class Trapecio : FormaGeometrica
    {
        public Trapecio(decimal ancho) : base(TipoForma.Trapecio, ancho)
        { }
    }
}