﻿using System.Collections.Generic;
using CodingChallenge.Data.Classes;
using NUnit.Framework;
using CodingChallenge.Data.Enums;
using FakeItEasy;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                Impresora.Imprimir(new List<FormaGeometrica>(), Idioma.Castellano));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                Impresora.Imprimir(new List<FormaGeometrica>(), Idioma.Ingles));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new List<FormaGeometrica> {FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 5)};

            var resumen = Impresora.Imprimir(cuadrados, Idioma.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnRectangulo()
        {
            var rectangulos = new List<FormaGeometrica> { FabricaFormaGeometrica.Fabricar(TipoForma.Rectangulo, 5, 4) };

            var resumen = Impresora.Imprimir(rectangulos, Idioma.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Rectángulo | Area 20 | Perimetro 18 <br/>TOTAL:<br/>1 formas Perimetro 18 Area 20", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new List<FormaGeometrica>
            {
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 5),
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 1),
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 3)
            };

            var resumen = Impresora.Imprimir(cuadrados, Idioma.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new List<FormaGeometrica>
            {
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 5),
                FabricaFormaGeometrica.Fabricar(TipoForma.Circulo, 3),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 4),
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 2),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 9),
                FabricaFormaGeometrica.Fabricar(TipoForma.Circulo, 2.75m),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 4.2m)
            };

            var resumen = Impresora.Imprimir(formas, Idioma.Ingles);

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 5),
                FabricaFormaGeometrica.Fabricar(TipoForma.Circulo, 3),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 4),
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 2),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 9),
                FabricaFormaGeometrica.Fabricar(TipoForma.Circulo, 2.75m),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 4.2m)
            };

            var resumen = Impresora.Imprimir(formas, Idioma.Castellano);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perimetro 28 <br/>2 Círculos | Area 13,01 | Perimetro 18,06 <br/>3 Triángulos | Area 49,64 | Perimetro 51,6 <br/>TOTAL:<br/>7 formas Perimetro 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnFrances()
        {
            var formas = new List<FormaGeometrica>
            {
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 5),
                FabricaFormaGeometrica.Fabricar(TipoForma.Circulo, 3),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 4),
                FabricaFormaGeometrica.Fabricar(TipoForma.Cuadrado, 2),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 9),
                FabricaFormaGeometrica.Fabricar(TipoForma.Circulo, 2.75m),
                FabricaFormaGeometrica.Fabricar(TipoForma.TrianguloEquilatero, 4.2m)
            };

            var resumen = Impresora.Imprimir(formas, Idioma.Frances);

            Assert.AreEqual(
                "<h1>Rapport de formes géométriques</h1>2 Carres | Surface 29 | Périmètre 28 <br/>2 Cercles | Surface 13,01 | Périmètre 18,06 <br/>3 Triangles | Surface 49,64 | Périmètre 51,6 <br/>TOTAL:<br/>7 formes Périmètre 97,66 Surface 91,65",
                resumen);
        }

        [TestCase]
        public void TestImprimirFormaNoSoportadaLanzaExcepcion()
        {
            var formas = new List<FormaGeometrica>();
            var forma = A.Fake<FormaGeometrica>();
            A.CallTo(() => forma.TipoForma).Returns(TipoForma.Trapecio);
            formas.Add(forma);

            Assert.Throws<System.Exception>(()=>Impresora.Imprimir(formas, Idioma.Castellano));
        }
    }
}
